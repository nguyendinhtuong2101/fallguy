﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Collections.IEnumerator Check_Screen::GetData()
extern void Check_Screen_GetData_m19F5FF20014B753EC249ECD0A5B4B1B471B98392 (void);
// 0x00000002 System.String Check_Screen::GetDataValue(System.String,System.String)
extern void Check_Screen_GetDataValue_mC243DE7296355CE0D8D69EC2DAE392264AC4BF98 (void);
// 0x00000003 System.Void Check_Screen::Start()
extern void Check_Screen_Start_m866A19CC272A2C072C211AFAC8419F6978F4E131 (void);
// 0x00000004 System.Void Check_Screen::SetupPortraitOrientation()
extern void Check_Screen_SetupPortraitOrientation_m31794DFAFD3ADBFD78A8B8F86B3A5EF3E39792C0 (void);
// 0x00000005 System.Void Check_Screen::SetupLandscapeOrientation()
extern void Check_Screen_SetupLandscapeOrientation_m965DC60E2FCA4CA6D89C3AC4D42E25C5009C4638 (void);
// 0x00000006 System.Void Check_Screen::.ctor()
extern void Check_Screen__ctor_mD16268D123331CD94B284D62DECD4EB16B1B976D (void);
// 0x00000007 System.Void Check_Screen/<GetData>d__2::.ctor(System.Int32)
extern void U3CGetDataU3Ed__2__ctor_m152CD56074327F9F09A19CE1F1EA2E1AE5758698 (void);
// 0x00000008 System.Void Check_Screen/<GetData>d__2::System.IDisposable.Dispose()
extern void U3CGetDataU3Ed__2_System_IDisposable_Dispose_m2D2CA36F6A0F995E4EB66C6A3D9FC163AE2016B3 (void);
// 0x00000009 System.Boolean Check_Screen/<GetData>d__2::MoveNext()
extern void U3CGetDataU3Ed__2_MoveNext_mCDEEA30D2C64B6227A8AA5B4084FBF8966B92260 (void);
// 0x0000000A System.Object Check_Screen/<GetData>d__2::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CGetDataU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m608B8FA108DD0C71AF1F962D27DBA16EB5F5DB45 (void);
// 0x0000000B System.Void Check_Screen/<GetData>d__2::System.Collections.IEnumerator.Reset()
extern void U3CGetDataU3Ed__2_System_Collections_IEnumerator_Reset_m016DAA27DEB41941DAFBB4F3A186DFFE6E3C72A4 (void);
// 0x0000000C System.Object Check_Screen/<GetData>d__2::System.Collections.IEnumerator.get_Current()
extern void U3CGetDataU3Ed__2_System_Collections_IEnumerator_get_Current_m83AFFE2D9E80B868F4252E00191B3EA64E214349 (void);
// 0x0000000D System.Void JoystickPlayerExample::FixedUpdate()
extern void JoystickPlayerExample_FixedUpdate_m9AEDBA111F95D67A006A5D3821956048224541B7 (void);
// 0x0000000E System.Void JoystickPlayerExample::.ctor()
extern void JoystickPlayerExample__ctor_m702422E0AE29402330CF41FDDBEE76F0506342E2 (void);
// 0x0000000F System.Void JoystickSetterExample::ModeChanged(System.Int32)
extern void JoystickSetterExample_ModeChanged_m35AF30EE3E6C8CEBF064A7AB80F5795E9AF06D23 (void);
// 0x00000010 System.Void JoystickSetterExample::AxisChanged(System.Int32)
extern void JoystickSetterExample_AxisChanged_m5CA220FEA14E06BD8A445E31C5B66E4601C5E404 (void);
// 0x00000011 System.Void JoystickSetterExample::SnapX(System.Boolean)
extern void JoystickSetterExample_SnapX_m25A77C3DE4C6DBBE3B4A58E2DE8CD44B1773D6A1 (void);
// 0x00000012 System.Void JoystickSetterExample::SnapY(System.Boolean)
extern void JoystickSetterExample_SnapY_m54FE8DCB2CE8D2BF5D2CDF84C68DE263F0E25B1B (void);
// 0x00000013 System.Void JoystickSetterExample::Update()
extern void JoystickSetterExample_Update_m99B2432D22FE669B4DC3209696AD4A62096C7D41 (void);
// 0x00000014 System.Void JoystickSetterExample::.ctor()
extern void JoystickSetterExample__ctor_m2A3D66E05BCDF78D0F116348094717BEBA73EC91 (void);
// 0x00000015 System.Single Joystick::get_Horizontal()
extern void Joystick_get_Horizontal_m78CF4472B86063E54254AC8AE0A52126E4008AFA (void);
// 0x00000016 System.Single Joystick::get_Vertical()
extern void Joystick_get_Vertical_mA2B0917896CF9CE47A6D342D1734E43441C3D4BE (void);
// 0x00000017 UnityEngine.Vector2 Joystick::get_Direction()
extern void Joystick_get_Direction_m52502695D42BDAB6075089BDE442ABE72EAC81EC (void);
// 0x00000018 System.Single Joystick::get_HandleRange()
extern void Joystick_get_HandleRange_mB38F0D3B6ADE2D1557D7A3D6759C469F17509D77 (void);
// 0x00000019 System.Void Joystick::set_HandleRange(System.Single)
extern void Joystick_set_HandleRange_m686B579A1F02EFCD4878BEA27EA606FC23CD2505 (void);
// 0x0000001A System.Single Joystick::get_DeadZone()
extern void Joystick_get_DeadZone_mCE52B635A8CF24F6DD867C14E34094515DE6AEFC (void);
// 0x0000001B System.Void Joystick::set_DeadZone(System.Single)
extern void Joystick_set_DeadZone_mD5699A14E5284026F303C8AF8D3457DFA9920F19 (void);
// 0x0000001C AxisOptions Joystick::get_AxisOptions()
extern void Joystick_get_AxisOptions_mA74F5FEE31C158E5179F0B108272ED28A661E388 (void);
// 0x0000001D System.Void Joystick::set_AxisOptions(AxisOptions)
extern void Joystick_set_AxisOptions_m541692280806ECF76F7C2C710973AF9D8AB334C6 (void);
// 0x0000001E System.Boolean Joystick::get_SnapX()
extern void Joystick_get_SnapX_m51CAFDCC399606BA82986908700AAA45668A0F40 (void);
// 0x0000001F System.Void Joystick::set_SnapX(System.Boolean)
extern void Joystick_set_SnapX_mB2090989F6AC933B30823751D74E799FC8D9B87A (void);
// 0x00000020 System.Boolean Joystick::get_SnapY()
extern void Joystick_get_SnapY_m35AFC1AD1DF5EDE5FCE8BAFEBE91AD51D7451613 (void);
// 0x00000021 System.Void Joystick::set_SnapY(System.Boolean)
extern void Joystick_set_SnapY_m7419D5EB972285A9E5E446CD668BEC266D11CD86 (void);
// 0x00000022 System.Void Joystick::Start()
extern void Joystick_Start_m5E46F090910AB69BE9838BFDB91A4F6E6934480C (void);
// 0x00000023 System.Void Joystick::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern void Joystick_OnPointerDown_mF176903D532D9129C90BBBD00FD7714BA3A0D8E6 (void);
// 0x00000024 System.Void Joystick::OnDrag(UnityEngine.EventSystems.PointerEventData)
extern void Joystick_OnDrag_m39E69636AEDC0E471EAD1371A99F4694ECDBA1E9 (void);
// 0x00000025 System.Void Joystick::HandleInput(System.Single,UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.Camera)
extern void Joystick_HandleInput_m15A4E86369A1AF0A4A5727DEC0FD93212A99901C (void);
// 0x00000026 System.Void Joystick::FormatInput()
extern void Joystick_FormatInput_mDDF7AF40138CF227F0106811C8749180FBF45342 (void);
// 0x00000027 System.Single Joystick::SnapFloat(System.Single,AxisOptions)
extern void Joystick_SnapFloat_mADE5AF21C67A2298A08CD12F9A8ED73AFA866987 (void);
// 0x00000028 System.Void Joystick::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern void Joystick_OnPointerUp_mEDED4DA77D954CBAC11CF82B57AB7A4DBFCDE22C (void);
// 0x00000029 UnityEngine.Vector2 Joystick::ScreenPointToAnchoredPosition(UnityEngine.Vector2)
extern void Joystick_ScreenPointToAnchoredPosition_mC1EB7560E844BF682674E4E7BD640604BC12B024 (void);
// 0x0000002A System.Void Joystick::.ctor()
extern void Joystick__ctor_m9BBE494CA4714F24227F33CB54C10B4DA78BF06B (void);
// 0x0000002B System.Single DynamicJoystick::get_MoveThreshold()
extern void DynamicJoystick_get_MoveThreshold_m16C670C1DA0A45E83F6F87C4304F459EDDEEDD5A (void);
// 0x0000002C System.Void DynamicJoystick::set_MoveThreshold(System.Single)
extern void DynamicJoystick_set_MoveThreshold_m000C1AE325C0B9C33172E4202F2AFB59820517F9 (void);
// 0x0000002D System.Void DynamicJoystick::Start()
extern void DynamicJoystick_Start_mFE16C6CE0B753F08E79A2AEC75782DEEF3B96F72 (void);
// 0x0000002E System.Void DynamicJoystick::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern void DynamicJoystick_OnPointerDown_mBFA3026A0DA4A6B53C0E747A1E892CBC7F43E136 (void);
// 0x0000002F System.Void DynamicJoystick::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern void DynamicJoystick_OnPointerUp_m10389907A9D3362F6B75FDC5F35AF37A5DD5AE7C (void);
// 0x00000030 System.Void DynamicJoystick::HandleInput(System.Single,UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.Camera)
extern void DynamicJoystick_HandleInput_m3F157F4825BE6682228C8E135581C6404D268506 (void);
// 0x00000031 System.Void DynamicJoystick::.ctor()
extern void DynamicJoystick__ctor_m9DDA6263314BD7B97679DF14A4664358BD3E58CB (void);
// 0x00000032 System.Void FixedJoystick::.ctor()
extern void FixedJoystick__ctor_m8C8BB74E5EA8CA2C3DD2AE084301EC91F519AD24 (void);
// 0x00000033 System.Void FloatingJoystick::Start()
extern void FloatingJoystick_Start_mB22059CD82AF77A8F94AC72E81A8BAE969399E81 (void);
// 0x00000034 System.Void FloatingJoystick::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern void FloatingJoystick_OnPointerDown_mFE5B4F54D5BBCA72F9729AB64765F558FA5C7A54 (void);
// 0x00000035 System.Void FloatingJoystick::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern void FloatingJoystick_OnPointerUp_m80ABA9C914E1953F91DBF74853CE84879352280D (void);
// 0x00000036 System.Void FloatingJoystick::.ctor()
extern void FloatingJoystick__ctor_m6B72425996D46B025F9E9D22121E9D01BEC6BD3C (void);
// 0x00000037 System.Single VariableJoystick::get_MoveThreshold()
extern void VariableJoystick_get_MoveThreshold_m8C9D3A63DB3B6CF1F0139C3504EC2E7AC4E7CF99 (void);
// 0x00000038 System.Void VariableJoystick::set_MoveThreshold(System.Single)
extern void VariableJoystick_set_MoveThreshold_m23DC4187B405EB690D297379E2113568B40C705A (void);
// 0x00000039 System.Void VariableJoystick::SetMode(JoystickType)
extern void VariableJoystick_SetMode_mB9D0B9B6E2E4F431E36AED6F5AC989305ADDB1EE (void);
// 0x0000003A System.Void VariableJoystick::Start()
extern void VariableJoystick_Start_m21743760641EA0317ACCD95949B99825446FE74D (void);
// 0x0000003B System.Void VariableJoystick::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern void VariableJoystick_OnPointerDown_m8ABE5C8EFBC8DB3A2EE135FFF3C0F67C533AF4B5 (void);
// 0x0000003C System.Void VariableJoystick::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern void VariableJoystick_OnPointerUp_m65296D82A6C2E1BDC2D622B9C922FAE8E4544526 (void);
// 0x0000003D System.Void VariableJoystick::HandleInput(System.Single,UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.Camera)
extern void VariableJoystick_HandleInput_mD1BCF9A068737A9C057EE8CEB7E6DEB682CC03AB (void);
// 0x0000003E System.Void VariableJoystick::.ctor()
extern void VariableJoystick__ctor_m6C7B41973BE20A94F16DB5DCC9AA805C5D8DF852 (void);
// 0x0000003F System.Void CameraShake::Start()
extern void CameraShake_Start_mDCEBFE0CD747A8BF83192C5356C727956F004E74 (void);
// 0x00000040 System.Void CameraShake::Awake()
extern void CameraShake_Awake_mB1B56CC9F67605372155A08678F17377AD9116AF (void);
// 0x00000041 System.Void CameraShake::Update()
extern void CameraShake_Update_m132373E37174BAA277442BF2CF8807BCA0600DB6 (void);
// 0x00000042 System.Void CameraShake::ShakeCamera(System.Single,System.Single)
extern void CameraShake_ShakeCamera_m486DAA30BA2E6543B15CD60F5A24F6E22ABC7E51 (void);
// 0x00000043 System.Void CameraShake::StopShake()
extern void CameraShake_StopShake_mBFB89AD03331D273F792E78CDDBEACAACE75ED01 (void);
// 0x00000044 System.Void CameraShake::.ctor()
extern void CameraShake__ctor_m55BC727470175313EB7C6F09E2EBD04610B8F325 (void);
// 0x00000045 System.Void Minimap::.ctor()
extern void Minimap__ctor_m9DA6BBEA6AE8120FD0EFEB167D3A1DC6DAFF2695 (void);
// 0x00000046 System.Void PlayerController::Start()
extern void PlayerController_Start_m1D83076E8B136A71051F2F02545EE04947D3A8CF (void);
// 0x00000047 System.Void PlayerController::FixedUpdate()
extern void PlayerController_FixedUpdate_m6D906D8B13844542B81CC49BA19760F747CEC8C0 (void);
// 0x00000048 System.Void PlayerController::.ctor()
extern void PlayerController__ctor_mDDAB7C7D82E1A5B3E6C197B1AB9D653DFE554F33 (void);
// 0x00000049 System.Boolean Boss::get_TargetVisible()
extern void Boss_get_TargetVisible_m3C13226A16ACED8C197667B8117398E4459FD745 (void);
// 0x0000004A System.Void Boss::set_TargetVisible(System.Boolean)
extern void Boss_set_TargetVisible_mD70968D785A4CC4C53141CF59A00707C23E0BD76 (void);
// 0x0000004B UnityEngine.Transform Boss::get_Target()
extern void Boss_get_Target_m4710357DA19B1B8686EC7DA1D9B191C093AC62F9 (void);
// 0x0000004C System.Void Boss::set_Target(UnityEngine.Transform)
extern void Boss_set_Target_m0E48CAE958F35F78D951563179CDDF5BBFA35636 (void);
// 0x0000004D System.Void Boss::Awake()
extern void Boss_Awake_mEF74D6D04EADA66FB78DE4C392525562C1930F2B (void);
// 0x0000004E System.Void Boss::Start()
extern void Boss_Start_mA6B8BA92DC170B01C128EE388C6D5C903A9829F9 (void);
// 0x0000004F UnityEngine.GameObject Boss::PlayerTarget()
extern void Boss_PlayerTarget_mDEEB6B521C13F33670C9AEE2ECEFA4F1E544551D (void);
// 0x00000050 UnityEngine.GameObject Boss::UnTarget()
extern void Boss_UnTarget_mF2BEFA2096B28D5D7D449A79F6A0FD331DC01E1C (void);
// 0x00000051 System.Void Boss::Update()
extern void Boss_Update_m1E41F05CD2AB4872C884423060661BF3EDDAAFB8 (void);
// 0x00000052 System.Void Boss::FixedUpdate()
extern void Boss_FixedUpdate_mB1C258973287C1772BBD854CB2133C98F0179BBE (void);
// 0x00000053 System.Boolean Boss::CheckTargetVisible()
extern void Boss_CheckTargetVisible_mAB4FE2CFB1B2C5B635D21484E637002D09DC8F10 (void);
// 0x00000054 System.Void Boss::DetectTarget()
extern void Boss_DetectTarget_m42808ED16DCA340FC5D1145399ACD0468111AD59 (void);
// 0x00000055 System.Collections.IEnumerator Boss::DetectionCoroutine()
extern void Boss_DetectionCoroutine_mDF9F16C6BDF3C59C69E2699403FE6EBD427D9347 (void);
// 0x00000056 System.Void Boss::DetectIfOutOfRange()
extern void Boss_DetectIfOutOfRange_m7EA4D53DF1CDEA47EF5F2A6E02244B5690BD4D71 (void);
// 0x00000057 System.Void Boss::CheckIfPlayerInRange()
extern void Boss_CheckIfPlayerInRange_mDC6A8AAE1C386C11B5715AB125E7973227AE4F3B (void);
// 0x00000058 System.Void Boss::OnCollisionEnter2D(UnityEngine.Collision2D)
extern void Boss_OnCollisionEnter2D_m78AA9CCCD720E3FFC254AC0B87ACC57705C3BA81 (void);
// 0x00000059 System.Void Boss::TakeDamage(System.Int32)
extern void Boss_TakeDamage_mE7433207DCA523FD10F9A4D706E78AC2C3C4286E (void);
// 0x0000005A System.Void Boss::.ctor()
extern void Boss__ctor_m000CFB50E293CE96995ADD0E5395D4B830DF7EAF (void);
// 0x0000005B System.Void Boss/<DetectionCoroutine>d__30::.ctor(System.Int32)
extern void U3CDetectionCoroutineU3Ed__30__ctor_m4F647E8ED41B20A28448DF9AC7138C4BB7EA2666 (void);
// 0x0000005C System.Void Boss/<DetectionCoroutine>d__30::System.IDisposable.Dispose()
extern void U3CDetectionCoroutineU3Ed__30_System_IDisposable_Dispose_m8505C82BDC504CB657A029978F28308EF43D6E05 (void);
// 0x0000005D System.Boolean Boss/<DetectionCoroutine>d__30::MoveNext()
extern void U3CDetectionCoroutineU3Ed__30_MoveNext_mD5EB3765033CCF5DCBEB513F8705991FB2D0E29B (void);
// 0x0000005E System.Object Boss/<DetectionCoroutine>d__30::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDetectionCoroutineU3Ed__30_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF7B8DD7A86D3856959C7AC8A42DDEEAF183BF4EB (void);
// 0x0000005F System.Void Boss/<DetectionCoroutine>d__30::System.Collections.IEnumerator.Reset()
extern void U3CDetectionCoroutineU3Ed__30_System_Collections_IEnumerator_Reset_m939484A408BDA306070DD5F11870ABC9319F9607 (void);
// 0x00000060 System.Object Boss/<DetectionCoroutine>d__30::System.Collections.IEnumerator.get_Current()
extern void U3CDetectionCoroutineU3Ed__30_System_Collections_IEnumerator_get_Current_m30F887716FC22B62D9902FA6A2867B90E02B3E58 (void);
// 0x00000061 System.Boolean Enemy::get_TargetVisible()
extern void Enemy_get_TargetVisible_mBA53A61B152D93534AEBECB434CC7BC593061274 (void);
// 0x00000062 System.Void Enemy::set_TargetVisible(System.Boolean)
extern void Enemy_set_TargetVisible_mB6A87747E438449F79EFEF3F66DAE17DE021001F (void);
// 0x00000063 UnityEngine.Transform Enemy::get_Target()
extern void Enemy_get_Target_m7E37944837C77E825AFB28FAE0B7AB75DD6BAB99 (void);
// 0x00000064 System.Void Enemy::set_Target(UnityEngine.Transform)
extern void Enemy_set_Target_mFD5DD5C0CB93C8BCAAB97B53E162D940B32A1363 (void);
// 0x00000065 System.Void Enemy::Awake()
extern void Enemy_Awake_mB58E74200229275689E6D9ADCDB6443D4E426624 (void);
// 0x00000066 System.Void Enemy::Start()
extern void Enemy_Start_m8BBD9A5AE10A27ABDFCD9168B93CD9C69D229034 (void);
// 0x00000067 UnityEngine.GameObject Enemy::PlayerTarget()
extern void Enemy_PlayerTarget_m89998388F75175268A3AE3DBBAEA4127EEF1BD59 (void);
// 0x00000068 UnityEngine.GameObject Enemy::UnTarget()
extern void Enemy_UnTarget_m4EEA11324F60D583FA06EC19C117BAB4612A6F86 (void);
// 0x00000069 System.Void Enemy::Update()
extern void Enemy_Update_m4149CFC3AC081AF0D654D9BDB6BC9B5540CE03D8 (void);
// 0x0000006A System.Void Enemy::FixedUpdate()
extern void Enemy_FixedUpdate_mC96E5B789335D23FC34546AD504EEBB0F054490B (void);
// 0x0000006B System.Boolean Enemy::CheckTargetVisible()
extern void Enemy_CheckTargetVisible_m2830DC71FF1D892412B67B72FC95CD960D1AEA44 (void);
// 0x0000006C System.Void Enemy::DetectTarget()
extern void Enemy_DetectTarget_mA33B9B2739A464FDD02625B04FF794D328422A79 (void);
// 0x0000006D System.Collections.IEnumerator Enemy::DetectionCoroutine()
extern void Enemy_DetectionCoroutine_mD07E9366C76B970AA33AB99AD4167D52DE861FFD (void);
// 0x0000006E System.Void Enemy::DetectIfOutOfRange()
extern void Enemy_DetectIfOutOfRange_m0B303DFC67F24B7DB340F34015AAF7ED4C1E7BC0 (void);
// 0x0000006F System.Void Enemy::CheckIfPlayerInRange()
extern void Enemy_CheckIfPlayerInRange_m381167169BD65F456FCE419FB168712DCE50DE1B (void);
// 0x00000070 System.Void Enemy::OnCollisionEnter2D(UnityEngine.Collision2D)
extern void Enemy_OnCollisionEnter2D_mCEA8628A3E777C0C06686FB5AE91FB49A94A1FA2 (void);
// 0x00000071 System.Void Enemy::TakeDamage(System.Int32)
extern void Enemy_TakeDamage_mFC3B669129AD870E4B66062CE726585528F8701D (void);
// 0x00000072 System.Void Enemy::.ctor()
extern void Enemy__ctor_mB6697627910F785A971C20C671DEFBA9D921D933 (void);
// 0x00000073 System.Void Enemy/<DetectionCoroutine>d__31::.ctor(System.Int32)
extern void U3CDetectionCoroutineU3Ed__31__ctor_m0EB52458516329FE64FC4A9780D2C8D682CAC9EB (void);
// 0x00000074 System.Void Enemy/<DetectionCoroutine>d__31::System.IDisposable.Dispose()
extern void U3CDetectionCoroutineU3Ed__31_System_IDisposable_Dispose_mAC81E2EC929326DC7D1A1CA12BB904FF8FF97AC8 (void);
// 0x00000075 System.Boolean Enemy/<DetectionCoroutine>d__31::MoveNext()
extern void U3CDetectionCoroutineU3Ed__31_MoveNext_m49EA391CC96BFEBC22C144AF10172A900FE85448 (void);
// 0x00000076 System.Object Enemy/<DetectionCoroutine>d__31::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDetectionCoroutineU3Ed__31_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4E0D82DDA0055852CCDA115624A6C71607DF652E (void);
// 0x00000077 System.Void Enemy/<DetectionCoroutine>d__31::System.Collections.IEnumerator.Reset()
extern void U3CDetectionCoroutineU3Ed__31_System_Collections_IEnumerator_Reset_m878F3C2C62F3B37E565BCB2AE3F532D7E0CFBC35 (void);
// 0x00000078 System.Object Enemy/<DetectionCoroutine>d__31::System.Collections.IEnumerator.get_Current()
extern void U3CDetectionCoroutineU3Ed__31_System_Collections_IEnumerator_get_Current_m94E9F85F09009F5470F750225F6D12291E9E2898 (void);
// 0x00000079 System.Void Hearth::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void Hearth_OnTriggerEnter2D_m48995A0560806BB27791FD2457F68825BA1CC568 (void);
// 0x0000007A System.Void Hearth::.ctor()
extern void Hearth__ctor_m3D157660959593805110FBE0FE7ABF1385537B85 (void);
// 0x0000007B System.Void Bullet::Start()
extern void Bullet_Start_m6BD187DD353835D248DA404B169DCE29CEB2B813 (void);
// 0x0000007C System.Void Bullet::Update()
extern void Bullet_Update_m5AA63D0B1F389C2CFEE77466E1C39ADC813B4DBC (void);
// 0x0000007D System.Void Bullet::MoveTowardsEnemy(UnityEngine.GameObject)
extern void Bullet_MoveTowardsEnemy_m314E141E2B73B185DFE36A68ACE6B523467E45E4 (void);
// 0x0000007E System.Void Bullet::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void Bullet_OnTriggerEnter2D_mC3642C0546BCF5AFC8036C6B77D66560C04685E8 (void);
// 0x0000007F System.Void Bullet::.ctor()
extern void Bullet__ctor_m873C02F2114EA93A35E4392013AC831246756CBA (void);
// 0x00000080 UnityEngine.GameObject ITarget::PlayerTarget()
// 0x00000081 UnityEngine.GameObject ITarget::UnTarget()
// 0x00000082 System.Void LaserShoot::Update()
extern void LaserShoot_Update_m0E1FEB6CD3EDB5879EDA2799F6A7378DAFD16AD3 (void);
// 0x00000083 System.Void LaserShoot::Draw2DRay(UnityEngine.Vector2,UnityEngine.Vector2)
extern void LaserShoot_Draw2DRay_m4601948D0B966C169AD4925D6D68D257AD9753FE (void);
// 0x00000084 System.Single LaserShoot::GetWaitingTime()
extern void LaserShoot_GetWaitingTime_mC6F58FFADCECF4628C7DB994BA8C8EDD6ED220C9 (void);
// 0x00000085 System.Void LaserShoot::SetWaitingTime(System.Single)
extern void LaserShoot_SetWaitingTime_mE3431CA6BE152893760880A0556A124EE6B7CDBE (void);
// 0x00000086 System.Boolean LaserShoot::CheckTargetVisible()
extern void LaserShoot_CheckTargetVisible_m047378AC517F3B2781B7BEE12ED528301B09143C (void);
// 0x00000087 System.Void LaserShoot::FindInArea()
extern void LaserShoot_FindInArea_m7436C30E8B41D24F378E050BCE68C0FAB4D84AD1 (void);
// 0x00000088 System.Void LaserShoot::OnDrawGizmos()
extern void LaserShoot_OnDrawGizmos_m0079DB1C68F35615DC7DE6B1C2344E08DB0C26C4 (void);
// 0x00000089 System.Void LaserShoot::.ctor()
extern void LaserShoot__ctor_m75F80B0B9962955972A16AB488BC53123AAD2A82 (void);
// 0x0000008A System.Void Shoot::Update()
extern void Shoot_Update_m8BAF074111542636D4F80A67B54B2AF3CFD14BFE (void);
// 0x0000008B System.Single Shoot::GetWaitingTime()
extern void Shoot_GetWaitingTime_mDC4084B91ADB2911CAE4B2923F7E65A0124089EB (void);
// 0x0000008C System.Void Shoot::SetWaitingTime(System.Single)
extern void Shoot_SetWaitingTime_m43CDE754F6C6C5C48CAD566353822200341F65E8 (void);
// 0x0000008D System.Boolean Shoot::CheckTargetVisible()
extern void Shoot_CheckTargetVisible_m2E8EFB91067667266086070DF809382B184D4438 (void);
// 0x0000008E System.Void Shoot::FindInArea()
extern void Shoot_FindInArea_mF2865548A1865CB950ED8D91AED88BE86CE18B38 (void);
// 0x0000008F System.Void Shoot::OnDrawGizmos()
extern void Shoot_OnDrawGizmos_m0C7D42FBC5144241E4E4383E6CBDF042F3FB104E (void);
// 0x00000090 System.Void Shoot::.ctor()
extern void Shoot__ctor_m9CC0333342223937F83B34DB4109B3101CCB3AA9 (void);
// 0x00000091 System.Void MapShuffle::.ctor()
extern void MapShuffle__ctor_mC5DE537178AFFAD6C98DFDE0303CAEFAE2AB99A0 (void);
// 0x00000092 System.Void Key::Start()
extern void Key_Start_m4623E79D02D7B947893DC95D623501BA70CAEAE4 (void);
// 0x00000093 System.Void Key::CalculateFloorsWeights(MapShuffle[])
extern void Key_CalculateFloorsWeights_m86607D9BA7634FD9C8FC8BB4615EC2A8E811D552 (void);
// 0x00000094 System.Int32 Key::GetRandomFloorIndex(MapShuffle[])
extern void Key_GetRandomFloorIndex_m4A6B12AAB8AAA1CADEB0C23B27F63321AAB06767 (void);
// 0x00000095 System.Void Key::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void Key_OnTriggerEnter2D_m75DD5CC1FA070E9F6602591A09F84EAEAB29B02D (void);
// 0x00000096 System.Void Key::.ctor()
extern void Key__ctor_mA069FB77FE5BAEC833A31356AB0867315F988954 (void);
// 0x00000097 System.Void GameMaster::StartRun()
extern void GameMaster_StartRun_m917A63CBE7FE01C1B4A5119D3F2CB690C4F3EF2E (void);
// 0x00000098 System.Void GameMaster::Restart()
extern void GameMaster_Restart_mC98984119018C50CC697BF9A72164A0AFF29E2FE (void);
// 0x00000099 System.Void GameMaster::GoToMenu()
extern void GameMaster_GoToMenu_mF977278377E4B46D9AF65C7B059CE3DDCF480827 (void);
// 0x0000009A System.Void GameMaster::.ctor()
extern void GameMaster__ctor_m129F9606E5D75C368420BE6B10EE572EE25E7780 (void);
// 0x0000009B System.Void Bomby::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void Bomby_OnTriggerEnter2D_m536B3A91C82F378AE52DA03F18EC5F621290790F (void);
// 0x0000009C System.Void Bomby::.ctor()
extern void Bomby__ctor_m1379BC4E1C157DB3BB5FF3FBD53869452EFDB93B (void);
// 0x0000009D System.Void BombyCollector::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void BombyCollector_OnTriggerEnter2D_mFD3DCCF7F141B7FFC5AB896C425ED80C9A5BCA4F (void);
// 0x0000009E System.Void BombyCollector::.ctor()
extern void BombyCollector__ctor_m72CB35B822E79A8D5476CF5601DEB590DA292119 (void);
// 0x0000009F System.Void DoubleShot::Start()
extern void DoubleShot_Start_mFB5E1166873122BC225E0EADF20019BE352EB5BD (void);
// 0x000000A0 System.Void DoubleShot::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void DoubleShot_OnTriggerEnter2D_m5559F17CC36F4EF8CD418BFB5B5E62831DAF7363 (void);
// 0x000000A1 System.Void DoubleShot::.ctor()
extern void DoubleShot__ctor_mCD290AF55EF2110EE4B3121ACA31EE758DB1D8DC (void);
// 0x000000A2 System.Void HealthUp::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void HealthUp_OnTriggerEnter2D_m86EACED2C3332DAD38ED9568C04F95C8E4FEFA9F (void);
// 0x000000A3 System.Void HealthUp::.ctor()
extern void HealthUp__ctor_mFB04723D1693953C7F06412875BB2AC600B0523A (void);
// 0x000000A4 System.Void TripleShot::Start()
extern void TripleShot_Start_m241265F81A29A0C6F8F5168EABF523D9E0CDD75C (void);
// 0x000000A5 System.Void TripleShot::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void TripleShot_OnTriggerEnter2D_mFFAE6E3F2010F1A550729702DE5A92B5A65BDCDE (void);
// 0x000000A6 System.Void TripleShot::.ctor()
extern void TripleShot__ctor_m9647E582D26A7217F8638CE49CFEB49172093D0E (void);
// 0x000000A7 System.Void Bot::Update()
extern void Bot_Update_m149B128C75D5A0157556DD8FCE3460EC91A972BA (void);
// 0x000000A8 System.Void Bot::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void Bot_OnTriggerEnter2D_mE8DDF07E1C018777FA23297F550136BA2F48C9ED (void);
// 0x000000A9 System.Void Bot::.ctor()
extern void Bot__ctor_m721C9F7B447E4CD482724E545EE797CD7762B2D8 (void);
// 0x000000AA System.Void HealthBar::SetMaxHealth(System.Int32)
extern void HealthBar_SetMaxHealth_m83136EA47EA9BE342810EB1344D4E4E7E7459CFE (void);
// 0x000000AB System.Void HealthBar::SetHealth(System.Int32)
extern void HealthBar_SetHealth_mC9D55DF6FDBB61FA5B5D05E3A3F8601F00045557 (void);
// 0x000000AC System.Void HealthBar::.ctor()
extern void HealthBar__ctor_m6874A2796BC8D86E80B24E349500653ACFA80662 (void);
// 0x000000AD System.Single Player::GetDifficultyScale()
extern void Player_GetDifficultyScale_mFD13FFF5BFC267C711C49E2911B007F3AFB5AAB4 (void);
// 0x000000AE System.Void Player::SetDifficultyScale(System.Single)
extern void Player_SetDifficultyScale_mFC60C2BBFAA5D5C51E3C272E7C8E746D07758585 (void);
// 0x000000AF System.Void Player::.ctor()
extern void Player__ctor_m0A83E0706592FC871B0CF188B37AFC6649F3D85D (void);
// 0x000000B0 System.Void PlayerHealth::Start()
extern void PlayerHealth_Start_mB40232C538ABF2AAA435E94E0A697D0A0D25C6F5 (void);
// 0x000000B1 System.Void PlayerHealth::Update()
extern void PlayerHealth_Update_m5645A1624A67A8F9332B3DB67A4C2BAABF3DCD5A (void);
// 0x000000B2 System.Void PlayerHealth::TakeDamage(System.Int32)
extern void PlayerHealth_TakeDamage_mB6C8B2F391600A980FC91EE61150A4C862E94071 (void);
// 0x000000B3 System.Void PlayerHealth::EndDamageAnimation()
extern void PlayerHealth_EndDamageAnimation_m382586AE134BB51CD58634BCFA75D789E2824B68 (void);
// 0x000000B4 System.Void PlayerHealth::PlayerDead()
extern void PlayerHealth_PlayerDead_mB571483D37F07C7B6DE2BDDEBD4A2E29869BCBBE (void);
// 0x000000B5 System.Void PlayerHealth::Heal(System.Int32)
extern void PlayerHealth_Heal_mD3007EA4440F16CA78A148830089AF07CFEAE9F0 (void);
// 0x000000B6 System.Void PlayerHealth::.ctor()
extern void PlayerHealth__ctor_m6A07958FCBF285AA65AB66D48C3EB198068F37BE (void);
// 0x000000B7 System.Void ChromaticAberrationEffect::Start()
extern void ChromaticAberrationEffect_Start_m488FF2D4820FFD24303768A9B59EFD127853830A (void);
// 0x000000B8 System.Void ChromaticAberrationEffect::FixedUpdate()
extern void ChromaticAberrationEffect_FixedUpdate_m29C2DED0C144B701F858C3A3E87CEC6E62EC6988 (void);
// 0x000000B9 System.Void ChromaticAberrationEffect::SetLowHealthEffect(System.Boolean)
extern void ChromaticAberrationEffect_SetLowHealthEffect_mA9ADB9F5D4F57E6772D34649D313BB007BFCA08C (void);
// 0x000000BA System.Void ChromaticAberrationEffect::.ctor()
extern void ChromaticAberrationEffect__ctor_m708269EC7FB79186558A2550169280C7D87F6108 (void);
// 0x000000BB System.Void AbstractDungeonGenerator::generateDungeon()
extern void AbstractDungeonGenerator_generateDungeon_m33E47C0DE0A0BA31EDB5078796870F640AF42120 (void);
// 0x000000BC System.Void AbstractDungeonGenerator::RunProceduralGeneration()
// 0x000000BD System.Void AbstractDungeonGenerator::.ctor()
extern void AbstractDungeonGenerator__ctor_mCE0757416D9F1F18B4D3CDC5A31FE679B1DA47C5 (void);
// 0x000000BE System.Void CorridorFirstDungeonGenerator::RunProceduralGeneration()
extern void CorridorFirstDungeonGenerator_RunProceduralGeneration_m285E9BA24D4A8F739B38251ED8DA633EF627E393 (void);
// 0x000000BF System.Void CorridorFirstDungeonGenerator::CorridorFirstGeneration()
extern void CorridorFirstDungeonGenerator_CorridorFirstGeneration_mF9F424A46F8ECE9412C088821AC40CAB651BE04A (void);
// 0x000000C0 System.Collections.Generic.List`1<System.Collections.Generic.List`1<UnityEngine.Vector2Int>> CorridorFirstDungeonGenerator::CreateCorridors(System.Collections.Generic.HashSet`1<UnityEngine.Vector2Int>,System.Collections.Generic.HashSet`1<UnityEngine.Vector2Int>)
extern void CorridorFirstDungeonGenerator_CreateCorridors_m0ADC4F2326FD51EF22D3545CAE388E859E6FDB5A (void);
// 0x000000C1 System.Collections.Generic.HashSet`1<UnityEngine.Vector2Int> CorridorFirstDungeonGenerator::CreateRooms(System.Collections.Generic.HashSet`1<UnityEngine.Vector2Int>)
extern void CorridorFirstDungeonGenerator_CreateRooms_m4F2085BB6B571B5BB0E3DC1AF39E072F0126175C (void);
// 0x000000C2 System.Collections.Generic.List`1<UnityEngine.Vector2Int> CorridorFirstDungeonGenerator::FindAllDeadEnds(System.Collections.Generic.HashSet`1<UnityEngine.Vector2Int>)
extern void CorridorFirstDungeonGenerator_FindAllDeadEnds_m31C2BA90FC231A3B024A84DF538794E514098179 (void);
// 0x000000C3 System.Void CorridorFirstDungeonGenerator::createRoomsAtDeadEnds(System.Collections.Generic.List`1<UnityEngine.Vector2Int>,System.Collections.Generic.HashSet`1<UnityEngine.Vector2Int>)
extern void CorridorFirstDungeonGenerator_createRoomsAtDeadEnds_m3F9B390B8BA6C3E5AFF30C3451732E0E57905765 (void);
// 0x000000C4 System.Collections.Generic.List`1<UnityEngine.Vector2Int> CorridorFirstDungeonGenerator::IncreaseCorridorBrush3by3(System.Collections.Generic.List`1<UnityEngine.Vector2Int>)
extern void CorridorFirstDungeonGenerator_IncreaseCorridorBrush3by3_m8788F580761639954CB8866DAF182E9D32993250 (void);
// 0x000000C5 System.Collections.Generic.List`1<UnityEngine.Vector2Int> CorridorFirstDungeonGenerator::IncreaseCorridorSizeByOne(System.Collections.Generic.List`1<UnityEngine.Vector2Int>)
extern void CorridorFirstDungeonGenerator_IncreaseCorridorSizeByOne_mCB0F31851BF8309E3167978C0EA277F81F6635F0 (void);
// 0x000000C6 UnityEngine.Vector2Int CorridorFirstDungeonGenerator::GetDirection90From(UnityEngine.Vector2Int)
extern void CorridorFirstDungeonGenerator_GetDirection90From_mBD2A0195396DFF8A0EF7DED68FB50206DBA53E36 (void);
// 0x000000C7 System.Void CorridorFirstDungeonGenerator::.ctor()
extern void CorridorFirstDungeonGenerator__ctor_mA7C7FBED6DED660D4120FD06885049EF864AA5DC (void);
// 0x000000C8 System.Void CorridorFirstDungeonGenerator/<>c::.cctor()
extern void U3CU3Ec__cctor_m109C4075252842EF4554EC34BEFAC0FAC31EBC47 (void);
// 0x000000C9 System.Void CorridorFirstDungeonGenerator/<>c::.ctor()
extern void U3CU3Ec__ctor_m95F885BD404F65400514146A50E67E0172F9F918 (void);
// 0x000000CA System.Guid CorridorFirstDungeonGenerator/<>c::<CreateRooms>b__9_0(UnityEngine.Vector2Int)
extern void U3CU3Ec_U3CCreateRoomsU3Eb__9_0_mC68FA7163D8D0AD8D500E391E4177B22D24C0A6B (void);
// 0x000000CB System.Void Graph::.ctor(System.Collections.Generic.IEnumerable`1<UnityEngine.Vector2Int>)
extern void Graph__ctor_m0CC674B780084DEDA6A8B4AF1EC1A6D73AB67B3D (void);
// 0x000000CC System.Collections.Generic.List`1<UnityEngine.Vector2Int> Graph::GetNeighbours4Directions(UnityEngine.Vector2Int)
extern void Graph_GetNeighbours4Directions_m1AB3E7336BBE2A73793738BD85702944BD76E980 (void);
// 0x000000CD System.Collections.Generic.List`1<UnityEngine.Vector2Int> Graph::GetNeighbours8Directions(UnityEngine.Vector2Int)
extern void Graph_GetNeighbours8Directions_m24D01B844493B8D9E50EC6770BCE12C4B81175E8 (void);
// 0x000000CE System.Collections.Generic.List`1<UnityEngine.Vector2Int> Graph::GetNeighbours(UnityEngine.Vector2Int,System.Collections.Generic.List`1<UnityEngine.Vector2Int>)
extern void Graph_GetNeighbours_mDD56848A68B6C2EFA293352903DBA92CB7829184 (void);
// 0x000000CF System.Void Graph::.cctor()
extern void Graph__cctor_mBF4BD31CB3515B47F131D17914A0E651EE25D17E (void);
// 0x000000D0 System.Void ItemPlacementHelper::CalculatePropsWeights(PropObject[])
extern void ItemPlacementHelper_CalculatePropsWeights_m22D70EEEAE512E6C311B41152B2A99354A37EE55 (void);
// 0x000000D1 System.Int32 ItemPlacementHelper::GetRandomPropIndex(PropObject[])
extern void ItemPlacementHelper_GetRandomPropIndex_m9BAFE4558B07436584EF573F0E092D3AFFD43B8D (void);
// 0x000000D2 System.Void ItemPlacementHelper::CalculatePerksWeights(Perk[])
extern void ItemPlacementHelper_CalculatePerksWeights_m74006E959AD9F19929A5314C1CC1178AA698267D (void);
// 0x000000D3 System.Int32 ItemPlacementHelper::GetRandomPerkIndex(Perk[])
extern void ItemPlacementHelper_GetRandomPerkIndex_mF38AE9355B216D395EB998793C215545D8FC11C5 (void);
// 0x000000D4 System.Void ItemPlacementHelper::ItemPlacementHelperMethod(System.Collections.Generic.HashSet`1<UnityEngine.Vector2Int>,System.Collections.Generic.HashSet`1<UnityEngine.Vector2Int>,PropObject[],System.Collections.Generic.HashSet`1<UnityEngine.Vector2Int>,System.Collections.Generic.List`1<UnityEngine.BoundsInt>,System.Collections.Generic.HashSet`1<UnityEngine.Vector2Int>,System.Collections.Generic.HashSet`1<UnityEngine.Vector2Int>,System.Collections.Generic.List`1<UnityEngine.GameObject>)
extern void ItemPlacementHelper_ItemPlacementHelperMethod_mDF744180E56252B8828628F12903D7314AA06269 (void);
// 0x000000D5 System.Void ItemPlacementHelper::PlaceItems(ItemPlacementHelper/PlacementType,PropObject[],UnityEngine.Vector2Int)
extern void ItemPlacementHelper_PlaceItems_m7D0E0134F29B21FE238861723FB69409613979AC (void);
// 0x000000D6 System.Void ItemPlacementHelper::PlaceEnemies(ItemPlacementHelper/PlacementType,UnityEngine.Vector2Int,System.Collections.Generic.List`1<UnityEngine.GameObject>)
extern void ItemPlacementHelper_PlaceEnemies_mF22397FC02447E3FF35F8C1A4A7561CDD17617C2 (void);
// 0x000000D7 System.Void ItemPlacementHelper::PlaceItemsBossRoom(UnityEngine.GameObject,UnityEngine.Vector2Int,UnityEngine.GameObject)
extern void ItemPlacementHelper_PlaceItemsBossRoom_mEBCC494932C7375F4F20994A5D8ECBC9B589CE88 (void);
// 0x000000D8 System.Void ItemPlacementHelper::PlaceItemsTreasureRoom(Perk[],UnityEngine.Vector2Int,UnityEngine.GameObject)
extern void ItemPlacementHelper_PlaceItemsTreasureRoom_m1A1284AA343BA48D004B2849A5B6D18C45243909 (void);
// 0x000000D9 System.Nullable`1<UnityEngine.Vector2> ItemPlacementHelper::GetItemPlacementPosition(ItemPlacementHelper/PlacementType,System.Int32,UnityEngine.Vector2Int,System.Boolean)
extern void ItemPlacementHelper_GetItemPlacementPosition_m4FD2B2DDFB24EBA36A346F5AF83A41F5EC01DD9E (void);
// 0x000000DA System.ValueTuple`2<System.Boolean,System.Collections.Generic.List`1<UnityEngine.Vector2Int>> ItemPlacementHelper::PlaceBigItem(UnityEngine.Vector2Int,UnityEngine.Vector2Int,System.Boolean)
extern void ItemPlacementHelper_PlaceBigItem_m53C12D3F1675E2605AE8642251881E8C383A6B78 (void);
// 0x000000DB System.Void ItemPlacementHelper::.ctor()
extern void ItemPlacementHelper__ctor_mB1053EAD2C21A54B13FDAF9CE54F4085108D2825 (void);
// 0x000000DC System.Collections.Generic.HashSet`1<UnityEngine.Vector2Int> ProceduralGenerationAlgorithms::SimpleRandomWalk(UnityEngine.Vector2Int,System.Int32)
extern void ProceduralGenerationAlgorithms_SimpleRandomWalk_mA86E251C63A809EB0C0D9F8817B3FBA3FD37D8CA (void);
// 0x000000DD System.Collections.Generic.List`1<UnityEngine.Vector2Int> ProceduralGenerationAlgorithms::RandomWalkCorridor(UnityEngine.Vector2Int,System.Int32)
extern void ProceduralGenerationAlgorithms_RandomWalkCorridor_mDD5EE35A69092651CDEF64D301AD0B16A89FE83F (void);
// 0x000000DE System.Collections.Generic.List`1<UnityEngine.BoundsInt> ProceduralGenerationAlgorithms::BinarySpacePartitioning(UnityEngine.BoundsInt,System.Int32,System.Int32)
extern void ProceduralGenerationAlgorithms_BinarySpacePartitioning_mB5110F742C19CD05DA8DA39AFC5A78AC115DA041 (void);
// 0x000000DF System.Void ProceduralGenerationAlgorithms::SplitVertically(System.Int32,System.Collections.Generic.Queue`1<UnityEngine.BoundsInt>,UnityEngine.BoundsInt)
extern void ProceduralGenerationAlgorithms_SplitVertically_m057DA90B738EF47E423792F4BDE48A72EC6E1D14 (void);
// 0x000000E0 System.Void ProceduralGenerationAlgorithms::SplitHorizontally(System.Int32,System.Collections.Generic.Queue`1<UnityEngine.BoundsInt>,UnityEngine.BoundsInt)
extern void ProceduralGenerationAlgorithms_SplitHorizontally_m7647279A23D579E24ACFAE9553B1DE797633C5DA (void);
// 0x000000E1 UnityEngine.Vector2Int Direction2D::GetRandomCardinalDirection()
extern void Direction2D_GetRandomCardinalDirection_m901DCB552E09E731ECC3D725219855F2A5F643C3 (void);
// 0x000000E2 System.Void Direction2D::.cctor()
extern void Direction2D__cctor_mFA24083F7C1303673AB4E278A748C49FD09B6291 (void);
// 0x000000E3 System.Void Prop::.ctor()
extern void Prop__ctor_m1A400EF58C3126BE12D9F4EC6677CE98B3A7B68C (void);
// 0x000000E4 System.Void PropObject::.ctor()
extern void PropObject__ctor_m59E31F445F024D72E44520B0261767BD49CDFE96 (void);
// 0x000000E5 System.Void Perk::.ctor()
extern void Perk__ctor_mC2121DCCC26538A8E812913855FD3EDC60574599 (void);
// 0x000000E6 System.Void RoomFirstDungeonGenerator::RunProceduralGeneration()
extern void RoomFirstDungeonGenerator_RunProceduralGeneration_m8A744CC1E15A8F350C06FA127FC2F8AC41BFDD0A (void);
// 0x000000E7 System.Void RoomFirstDungeonGenerator::Start()
extern void RoomFirstDungeonGenerator_Start_mA5F92B643D924EF3F2957F8062023E68C233FC69 (void);
// 0x000000E8 System.Void RoomFirstDungeonGenerator::ClearAll()
extern void RoomFirstDungeonGenerator_ClearAll_m26E5CB4F2D95A3FB31511556FF2CAB3015FA6E70 (void);
// 0x000000E9 System.Void RoomFirstDungeonGenerator::CreateRooms()
extern void RoomFirstDungeonGenerator_CreateRooms_mB5ED554CF6ECE2817143FA596F1D353217089347 (void);
// 0x000000EA System.Collections.Generic.HashSet`1<UnityEngine.Vector2Int> RoomFirstDungeonGenerator::CreateSimpleRooms(System.Collections.Generic.List`1<UnityEngine.BoundsInt>)
extern void RoomFirstDungeonGenerator_CreateSimpleRooms_m8FDFB429853180AD2086F8D1A5B1FD20653DDCF6 (void);
// 0x000000EB System.Collections.Generic.HashSet`1<UnityEngine.Vector2Int> RoomFirstDungeonGenerator::ConnectRooms(System.Collections.Generic.List`1<UnityEngine.Vector2Int>)
extern void RoomFirstDungeonGenerator_ConnectRooms_m25C56F22BDB247E12BA46E54B786BF3D1F63FBE4 (void);
// 0x000000EC UnityEngine.Vector2Int RoomFirstDungeonGenerator::FindClosestPointTo(UnityEngine.Vector2Int,System.Collections.Generic.List`1<UnityEngine.Vector2Int>)
extern void RoomFirstDungeonGenerator_FindClosestPointTo_m7C3284DB259D6791026BDBA2DC34E40432E69637 (void);
// 0x000000ED System.Collections.Generic.HashSet`1<UnityEngine.Vector2Int> RoomFirstDungeonGenerator::CreateCorridor(UnityEngine.Vector2Int,UnityEngine.Vector2Int)
extern void RoomFirstDungeonGenerator_CreateCorridor_mF7D2EF8E14C44FB52AC3BDB6E7A561DE3CA23659 (void);
// 0x000000EE UnityEngine.Vector2Int RoomFirstDungeonGenerator::SpawnPlayer(UnityEngine.Vector2Int,UnityEngine.GameObject)
extern void RoomFirstDungeonGenerator_SpawnPlayer_m8B51B268B805745B862DA85CBE3BB6A487180484 (void);
// 0x000000EF System.Collections.Generic.HashSet`1<UnityEngine.Vector2Int> RoomFirstDungeonGenerator::IncreaseCorridorSize(System.Collections.Generic.HashSet`1<UnityEngine.Vector2Int>)
extern void RoomFirstDungeonGenerator_IncreaseCorridorSize_m4D3C35252832C4A6C5BBB009025A129E6625C987 (void);
// 0x000000F0 System.Void RoomFirstDungeonGenerator::.ctor()
extern void RoomFirstDungeonGenerator__ctor_m4D3C06DD867BFE1C1ED46C2F72DA91D9A4BEAA1A (void);
// 0x000000F1 System.Void SimpleRandomWalkMapGenerator::RunProceduralGeneration()
extern void SimpleRandomWalkMapGenerator_RunProceduralGeneration_m0647C1D0F5F981CA414170B99B00B425E2759C74 (void);
// 0x000000F2 System.Collections.Generic.HashSet`1<UnityEngine.Vector2Int> SimpleRandomWalkMapGenerator::RunRandomWalk(System.Int32,System.Int32,System.Boolean,UnityEngine.Vector2Int)
extern void SimpleRandomWalkMapGenerator_RunRandomWalk_m53DFBA6F75960C52C21FB38CC9CA8188CFB22AE8 (void);
// 0x000000F3 System.Void SimpleRandomWalkMapGenerator::.ctor()
extern void SimpleRandomWalkMapGenerator__ctor_m9A17F3808C2E6040C37AFFBB169C310AC8F3518C (void);
// 0x000000F4 System.Void Floor::.ctor()
extern void Floor__ctor_mC6DD1BA9604B35FFF0346F67AA3D390F4C2439BF (void);
// 0x000000F5 System.Void TilemapVisualizer::Awake()
extern void TilemapVisualizer_Awake_m41E05327E14B2C073ECBE54207A3C1B0682DB762 (void);
// 0x000000F6 System.Int32 TilemapVisualizer::GetRandomFloorIndex()
extern void TilemapVisualizer_GetRandomFloorIndex_mCD55AE14DC05DFC62D814F2823F391105E52F257 (void);
// 0x000000F7 System.Void TilemapVisualizer::CalculateFloorWeights()
extern void TilemapVisualizer_CalculateFloorWeights_m5E58C6DEE2FB5C012DC3749BA8B64860951CB535 (void);
// 0x000000F8 System.Void TilemapVisualizer::PaintFloorTiles(System.Collections.Generic.IEnumerable`1<UnityEngine.Vector2Int>)
extern void TilemapVisualizer_PaintFloorTiles_mA7DFAC26723F24E9AF3C2C8A908A3358DF8DDBDD (void);
// 0x000000F9 System.Void TilemapVisualizer::PaintTiles(System.Collections.Generic.IEnumerable`1<UnityEngine.Vector2Int>,UnityEngine.Tilemaps.Tilemap,UnityEngine.Tilemaps.TileBase)
extern void TilemapVisualizer_PaintTiles_mAEA1043A45B5803065E0A1E6372C591ED0493481 (void);
// 0x000000FA System.Void TilemapVisualizer::PaintSingleTile(UnityEngine.Tilemaps.Tilemap,UnityEngine.Tilemaps.TileBase,UnityEngine.Vector2Int)
extern void TilemapVisualizer_PaintSingleTile_m02572F581FF4DB5D993C60CBE47266A4E5A0EB36 (void);
// 0x000000FB System.Void TilemapVisualizer::PaintSingleBasicWall(UnityEngine.Vector2Int,System.String)
extern void TilemapVisualizer_PaintSingleBasicWall_mCD34D4B47C1840E29E6D81EAEFEBE32954571107 (void);
// 0x000000FC System.Void TilemapVisualizer::PaintSingleCornerWall(UnityEngine.Vector2Int,System.String)
extern void TilemapVisualizer_PaintSingleCornerWall_m1914D8596949EC2AA559379217616BEA4848286D (void);
// 0x000000FD System.Void TilemapVisualizer::Clear()
extern void TilemapVisualizer_Clear_mA94CE31652588043DADF56CC4739F3BB21968A40 (void);
// 0x000000FE System.Void TilemapVisualizer::.ctor()
extern void TilemapVisualizer__ctor_mFFEC8C1476753A323D801584347257CB00740278 (void);
// 0x000000FF System.Void WallGenerator::CreateWalls(System.Collections.Generic.HashSet`1<UnityEngine.Vector2Int>,TilemapVisualizer)
extern void WallGenerator_CreateWalls_mE5288EEA53D08F56C2A6BC7AC1CC39F6A4D01C83 (void);
// 0x00000100 System.Void WallGenerator::CreateBasicWalls(TilemapVisualizer,System.Collections.Generic.HashSet`1<UnityEngine.Vector2Int>,System.Collections.Generic.HashSet`1<UnityEngine.Vector2Int>)
extern void WallGenerator_CreateBasicWalls_m972B9E06431C61DF8308524C7DFE501CA96B6F33 (void);
// 0x00000101 System.Void WallGenerator::CreateCornerWalls(TilemapVisualizer,System.Collections.Generic.HashSet`1<UnityEngine.Vector2Int>,System.Collections.Generic.HashSet`1<UnityEngine.Vector2Int>)
extern void WallGenerator_CreateCornerWalls_mEE4872F704D73B0D0B86541C2358C7BF97486D0A (void);
// 0x00000102 System.Collections.Generic.HashSet`1<UnityEngine.Vector2Int> WallGenerator::FindWallsInDirections(System.Collections.Generic.HashSet`1<UnityEngine.Vector2Int>,System.Collections.Generic.List`1<UnityEngine.Vector2Int>)
extern void WallGenerator_FindWallsInDirections_m42CD3AD9D4DE37EA80270F18A72172FB0D9E2E6C (void);
// 0x00000103 System.Void WallTypesHelper::.cctor()
extern void WallTypesHelper__cctor_m9E1D60C507AD9C523D9BFD6E3CC0ADA93724AACE (void);
// 0x00000104 System.Void Chest::Start()
extern void Chest_Start_m13FCC9702F8A7CC4FEA2847E6E3CAB7A635B1DCF (void);
// 0x00000105 System.Void Chest::TakeDamage(System.Int32)
extern void Chest_TakeDamage_m189E71FBA172DF441FA974FEA2D9895AC30D81E9 (void);
// 0x00000106 System.Void Chest::.ctor()
extern void Chest__ctor_m2D31AFE0D73A6DF61B1F7EAAB809D3E1F6FF579A (void);
// 0x00000107 System.Void Coin::Start()
extern void Coin_Start_mA073768C06B75276A262C088D118DEEE9F40991E (void);
// 0x00000108 System.Void Coin::Update()
extern void Coin_Update_mD81A56D3C82C2FF349BB57ED6C8B95699F2E5908 (void);
// 0x00000109 System.Void Coin::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void Coin_OnTriggerEnter2D_m3983AB890902F168831629F16D99A3C0A6160448 (void);
// 0x0000010A System.Void Coin::.ctor()
extern void Coin__ctor_mFEAAC42C1DAABB5CCCF4B39FCDBC5D0B0B8A183D (void);
// 0x0000010B System.Void CoinSystem::Start()
extern void CoinSystem_Start_m1E1E87A571602BF33CB8D7950B333E785779186D (void);
// 0x0000010C System.Void CoinSystem::.ctor()
extern void CoinSystem__ctor_m91E7A20F6037BCC110349B4621A04F70C91DD24D (void);
// 0x0000010D System.Void Magnet::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void Magnet_OnTriggerEnter2D_mA988BB37218E5DC4FA79815D1EE748EC366FA749 (void);
// 0x0000010E System.Void Magnet::.ctor()
extern void Magnet__ctor_m3B1CFB312BF10846408B5F0255B5E76C747B1812 (void);
// 0x0000010F System.Void DigitalRuby.LightningBolt.LightningBoltScript::GetPerpendicularVector(UnityEngine.Vector3&,UnityEngine.Vector3&)
extern void LightningBoltScript_GetPerpendicularVector_m6E14CE8B174161B94706CE092FF3BE4049C53983 (void);
// 0x00000110 System.Void DigitalRuby.LightningBolt.LightningBoltScript::GenerateLightningBolt(UnityEngine.Vector3,UnityEngine.Vector3,System.Int32,System.Int32,System.Single)
extern void LightningBoltScript_GenerateLightningBolt_m6F45F784BE0610F9E9B519C5E290D3515DE0BA49 (void);
// 0x00000111 System.Void DigitalRuby.LightningBolt.LightningBoltScript::RandomVector(UnityEngine.Vector3&,UnityEngine.Vector3&,System.Single,UnityEngine.Vector3&)
extern void LightningBoltScript_RandomVector_m8D5CB399D3CE0038571D41DE824CE673E766B4FA (void);
// 0x00000112 System.Void DigitalRuby.LightningBolt.LightningBoltScript::SelectOffsetFromAnimationMode()
extern void LightningBoltScript_SelectOffsetFromAnimationMode_m98F0C804C05EFB6BC3283AC50D654D5A672B6199 (void);
// 0x00000113 System.Void DigitalRuby.LightningBolt.LightningBoltScript::UpdateLineRenderer()
extern void LightningBoltScript_UpdateLineRenderer_mC709724D71E0E95242108C4B9D6F7DA6BAB71F65 (void);
// 0x00000114 System.Void DigitalRuby.LightningBolt.LightningBoltScript::Start()
extern void LightningBoltScript_Start_m980C378E5754957B1F4DE221515969B6A328240E (void);
// 0x00000115 System.Void DigitalRuby.LightningBolt.LightningBoltScript::Update()
extern void LightningBoltScript_Update_m0BE2BF4FB4CD027E8C78FE58991FFF103836AF31 (void);
// 0x00000116 System.Void DigitalRuby.LightningBolt.LightningBoltScript::Trigger()
extern void LightningBoltScript_Trigger_mA1EDB81D4D9001D2B19FFBACB0A36BAFEE96C284 (void);
// 0x00000117 System.Void DigitalRuby.LightningBolt.LightningBoltScript::UpdateFromMaterialChange()
extern void LightningBoltScript_UpdateFromMaterialChange_m977EBCF7F3B1B47A777F34B0305C7FC8A04E1EF5 (void);
// 0x00000118 System.Void DigitalRuby.LightningBolt.LightningBoltScript::.ctor()
extern void LightningBoltScript__ctor_m6B404ACF4FA73DFC000CE86270A4409AA1BBDD42 (void);
static Il2CppMethodPointer s_methodPointers[280] = 
{
	Check_Screen_GetData_m19F5FF20014B753EC249ECD0A5B4B1B471B98392,
	Check_Screen_GetDataValue_mC243DE7296355CE0D8D69EC2DAE392264AC4BF98,
	Check_Screen_Start_m866A19CC272A2C072C211AFAC8419F6978F4E131,
	Check_Screen_SetupPortraitOrientation_m31794DFAFD3ADBFD78A8B8F86B3A5EF3E39792C0,
	Check_Screen_SetupLandscapeOrientation_m965DC60E2FCA4CA6D89C3AC4D42E25C5009C4638,
	Check_Screen__ctor_mD16268D123331CD94B284D62DECD4EB16B1B976D,
	U3CGetDataU3Ed__2__ctor_m152CD56074327F9F09A19CE1F1EA2E1AE5758698,
	U3CGetDataU3Ed__2_System_IDisposable_Dispose_m2D2CA36F6A0F995E4EB66C6A3D9FC163AE2016B3,
	U3CGetDataU3Ed__2_MoveNext_mCDEEA30D2C64B6227A8AA5B4084FBF8966B92260,
	U3CGetDataU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m608B8FA108DD0C71AF1F962D27DBA16EB5F5DB45,
	U3CGetDataU3Ed__2_System_Collections_IEnumerator_Reset_m016DAA27DEB41941DAFBB4F3A186DFFE6E3C72A4,
	U3CGetDataU3Ed__2_System_Collections_IEnumerator_get_Current_m83AFFE2D9E80B868F4252E00191B3EA64E214349,
	JoystickPlayerExample_FixedUpdate_m9AEDBA111F95D67A006A5D3821956048224541B7,
	JoystickPlayerExample__ctor_m702422E0AE29402330CF41FDDBEE76F0506342E2,
	JoystickSetterExample_ModeChanged_m35AF30EE3E6C8CEBF064A7AB80F5795E9AF06D23,
	JoystickSetterExample_AxisChanged_m5CA220FEA14E06BD8A445E31C5B66E4601C5E404,
	JoystickSetterExample_SnapX_m25A77C3DE4C6DBBE3B4A58E2DE8CD44B1773D6A1,
	JoystickSetterExample_SnapY_m54FE8DCB2CE8D2BF5D2CDF84C68DE263F0E25B1B,
	JoystickSetterExample_Update_m99B2432D22FE669B4DC3209696AD4A62096C7D41,
	JoystickSetterExample__ctor_m2A3D66E05BCDF78D0F116348094717BEBA73EC91,
	Joystick_get_Horizontal_m78CF4472B86063E54254AC8AE0A52126E4008AFA,
	Joystick_get_Vertical_mA2B0917896CF9CE47A6D342D1734E43441C3D4BE,
	Joystick_get_Direction_m52502695D42BDAB6075089BDE442ABE72EAC81EC,
	Joystick_get_HandleRange_mB38F0D3B6ADE2D1557D7A3D6759C469F17509D77,
	Joystick_set_HandleRange_m686B579A1F02EFCD4878BEA27EA606FC23CD2505,
	Joystick_get_DeadZone_mCE52B635A8CF24F6DD867C14E34094515DE6AEFC,
	Joystick_set_DeadZone_mD5699A14E5284026F303C8AF8D3457DFA9920F19,
	Joystick_get_AxisOptions_mA74F5FEE31C158E5179F0B108272ED28A661E388,
	Joystick_set_AxisOptions_m541692280806ECF76F7C2C710973AF9D8AB334C6,
	Joystick_get_SnapX_m51CAFDCC399606BA82986908700AAA45668A0F40,
	Joystick_set_SnapX_mB2090989F6AC933B30823751D74E799FC8D9B87A,
	Joystick_get_SnapY_m35AFC1AD1DF5EDE5FCE8BAFEBE91AD51D7451613,
	Joystick_set_SnapY_m7419D5EB972285A9E5E446CD668BEC266D11CD86,
	Joystick_Start_m5E46F090910AB69BE9838BFDB91A4F6E6934480C,
	Joystick_OnPointerDown_mF176903D532D9129C90BBBD00FD7714BA3A0D8E6,
	Joystick_OnDrag_m39E69636AEDC0E471EAD1371A99F4694ECDBA1E9,
	Joystick_HandleInput_m15A4E86369A1AF0A4A5727DEC0FD93212A99901C,
	Joystick_FormatInput_mDDF7AF40138CF227F0106811C8749180FBF45342,
	Joystick_SnapFloat_mADE5AF21C67A2298A08CD12F9A8ED73AFA866987,
	Joystick_OnPointerUp_mEDED4DA77D954CBAC11CF82B57AB7A4DBFCDE22C,
	Joystick_ScreenPointToAnchoredPosition_mC1EB7560E844BF682674E4E7BD640604BC12B024,
	Joystick__ctor_m9BBE494CA4714F24227F33CB54C10B4DA78BF06B,
	DynamicJoystick_get_MoveThreshold_m16C670C1DA0A45E83F6F87C4304F459EDDEEDD5A,
	DynamicJoystick_set_MoveThreshold_m000C1AE325C0B9C33172E4202F2AFB59820517F9,
	DynamicJoystick_Start_mFE16C6CE0B753F08E79A2AEC75782DEEF3B96F72,
	DynamicJoystick_OnPointerDown_mBFA3026A0DA4A6B53C0E747A1E892CBC7F43E136,
	DynamicJoystick_OnPointerUp_m10389907A9D3362F6B75FDC5F35AF37A5DD5AE7C,
	DynamicJoystick_HandleInput_m3F157F4825BE6682228C8E135581C6404D268506,
	DynamicJoystick__ctor_m9DDA6263314BD7B97679DF14A4664358BD3E58CB,
	FixedJoystick__ctor_m8C8BB74E5EA8CA2C3DD2AE084301EC91F519AD24,
	FloatingJoystick_Start_mB22059CD82AF77A8F94AC72E81A8BAE969399E81,
	FloatingJoystick_OnPointerDown_mFE5B4F54D5BBCA72F9729AB64765F558FA5C7A54,
	FloatingJoystick_OnPointerUp_m80ABA9C914E1953F91DBF74853CE84879352280D,
	FloatingJoystick__ctor_m6B72425996D46B025F9E9D22121E9D01BEC6BD3C,
	VariableJoystick_get_MoveThreshold_m8C9D3A63DB3B6CF1F0139C3504EC2E7AC4E7CF99,
	VariableJoystick_set_MoveThreshold_m23DC4187B405EB690D297379E2113568B40C705A,
	VariableJoystick_SetMode_mB9D0B9B6E2E4F431E36AED6F5AC989305ADDB1EE,
	VariableJoystick_Start_m21743760641EA0317ACCD95949B99825446FE74D,
	VariableJoystick_OnPointerDown_m8ABE5C8EFBC8DB3A2EE135FFF3C0F67C533AF4B5,
	VariableJoystick_OnPointerUp_m65296D82A6C2E1BDC2D622B9C922FAE8E4544526,
	VariableJoystick_HandleInput_mD1BCF9A068737A9C057EE8CEB7E6DEB682CC03AB,
	VariableJoystick__ctor_m6C7B41973BE20A94F16DB5DCC9AA805C5D8DF852,
	CameraShake_Start_mDCEBFE0CD747A8BF83192C5356C727956F004E74,
	CameraShake_Awake_mB1B56CC9F67605372155A08678F17377AD9116AF,
	CameraShake_Update_m132373E37174BAA277442BF2CF8807BCA0600DB6,
	CameraShake_ShakeCamera_m486DAA30BA2E6543B15CD60F5A24F6E22ABC7E51,
	CameraShake_StopShake_mBFB89AD03331D273F792E78CDDBEACAACE75ED01,
	CameraShake__ctor_m55BC727470175313EB7C6F09E2EBD04610B8F325,
	Minimap__ctor_m9DA6BBEA6AE8120FD0EFEB167D3A1DC6DAFF2695,
	PlayerController_Start_m1D83076E8B136A71051F2F02545EE04947D3A8CF,
	PlayerController_FixedUpdate_m6D906D8B13844542B81CC49BA19760F747CEC8C0,
	PlayerController__ctor_mDDAB7C7D82E1A5B3E6C197B1AB9D653DFE554F33,
	Boss_get_TargetVisible_m3C13226A16ACED8C197667B8117398E4459FD745,
	Boss_set_TargetVisible_mD70968D785A4CC4C53141CF59A00707C23E0BD76,
	Boss_get_Target_m4710357DA19B1B8686EC7DA1D9B191C093AC62F9,
	Boss_set_Target_m0E48CAE958F35F78D951563179CDDF5BBFA35636,
	Boss_Awake_mEF74D6D04EADA66FB78DE4C392525562C1930F2B,
	Boss_Start_mA6B8BA92DC170B01C128EE388C6D5C903A9829F9,
	Boss_PlayerTarget_mDEEB6B521C13F33670C9AEE2ECEFA4F1E544551D,
	Boss_UnTarget_mF2BEFA2096B28D5D7D449A79F6A0FD331DC01E1C,
	Boss_Update_m1E41F05CD2AB4872C884423060661BF3EDDAAFB8,
	Boss_FixedUpdate_mB1C258973287C1772BBD854CB2133C98F0179BBE,
	Boss_CheckTargetVisible_mAB4FE2CFB1B2C5B635D21484E637002D09DC8F10,
	Boss_DetectTarget_m42808ED16DCA340FC5D1145399ACD0468111AD59,
	Boss_DetectionCoroutine_mDF9F16C6BDF3C59C69E2699403FE6EBD427D9347,
	Boss_DetectIfOutOfRange_m7EA4D53DF1CDEA47EF5F2A6E02244B5690BD4D71,
	Boss_CheckIfPlayerInRange_mDC6A8AAE1C386C11B5715AB125E7973227AE4F3B,
	Boss_OnCollisionEnter2D_m78AA9CCCD720E3FFC254AC0B87ACC57705C3BA81,
	Boss_TakeDamage_mE7433207DCA523FD10F9A4D706E78AC2C3C4286E,
	Boss__ctor_m000CFB50E293CE96995ADD0E5395D4B830DF7EAF,
	U3CDetectionCoroutineU3Ed__30__ctor_m4F647E8ED41B20A28448DF9AC7138C4BB7EA2666,
	U3CDetectionCoroutineU3Ed__30_System_IDisposable_Dispose_m8505C82BDC504CB657A029978F28308EF43D6E05,
	U3CDetectionCoroutineU3Ed__30_MoveNext_mD5EB3765033CCF5DCBEB513F8705991FB2D0E29B,
	U3CDetectionCoroutineU3Ed__30_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF7B8DD7A86D3856959C7AC8A42DDEEAF183BF4EB,
	U3CDetectionCoroutineU3Ed__30_System_Collections_IEnumerator_Reset_m939484A408BDA306070DD5F11870ABC9319F9607,
	U3CDetectionCoroutineU3Ed__30_System_Collections_IEnumerator_get_Current_m30F887716FC22B62D9902FA6A2867B90E02B3E58,
	Enemy_get_TargetVisible_mBA53A61B152D93534AEBECB434CC7BC593061274,
	Enemy_set_TargetVisible_mB6A87747E438449F79EFEF3F66DAE17DE021001F,
	Enemy_get_Target_m7E37944837C77E825AFB28FAE0B7AB75DD6BAB99,
	Enemy_set_Target_mFD5DD5C0CB93C8BCAAB97B53E162D940B32A1363,
	Enemy_Awake_mB58E74200229275689E6D9ADCDB6443D4E426624,
	Enemy_Start_m8BBD9A5AE10A27ABDFCD9168B93CD9C69D229034,
	Enemy_PlayerTarget_m89998388F75175268A3AE3DBBAEA4127EEF1BD59,
	Enemy_UnTarget_m4EEA11324F60D583FA06EC19C117BAB4612A6F86,
	Enemy_Update_m4149CFC3AC081AF0D654D9BDB6BC9B5540CE03D8,
	Enemy_FixedUpdate_mC96E5B789335D23FC34546AD504EEBB0F054490B,
	Enemy_CheckTargetVisible_m2830DC71FF1D892412B67B72FC95CD960D1AEA44,
	Enemy_DetectTarget_mA33B9B2739A464FDD02625B04FF794D328422A79,
	Enemy_DetectionCoroutine_mD07E9366C76B970AA33AB99AD4167D52DE861FFD,
	Enemy_DetectIfOutOfRange_m0B303DFC67F24B7DB340F34015AAF7ED4C1E7BC0,
	Enemy_CheckIfPlayerInRange_m381167169BD65F456FCE419FB168712DCE50DE1B,
	Enemy_OnCollisionEnter2D_mCEA8628A3E777C0C06686FB5AE91FB49A94A1FA2,
	Enemy_TakeDamage_mFC3B669129AD870E4B66062CE726585528F8701D,
	Enemy__ctor_mB6697627910F785A971C20C671DEFBA9D921D933,
	U3CDetectionCoroutineU3Ed__31__ctor_m0EB52458516329FE64FC4A9780D2C8D682CAC9EB,
	U3CDetectionCoroutineU3Ed__31_System_IDisposable_Dispose_mAC81E2EC929326DC7D1A1CA12BB904FF8FF97AC8,
	U3CDetectionCoroutineU3Ed__31_MoveNext_m49EA391CC96BFEBC22C144AF10172A900FE85448,
	U3CDetectionCoroutineU3Ed__31_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4E0D82DDA0055852CCDA115624A6C71607DF652E,
	U3CDetectionCoroutineU3Ed__31_System_Collections_IEnumerator_Reset_m878F3C2C62F3B37E565BCB2AE3F532D7E0CFBC35,
	U3CDetectionCoroutineU3Ed__31_System_Collections_IEnumerator_get_Current_m94E9F85F09009F5470F750225F6D12291E9E2898,
	Hearth_OnTriggerEnter2D_m48995A0560806BB27791FD2457F68825BA1CC568,
	Hearth__ctor_m3D157660959593805110FBE0FE7ABF1385537B85,
	Bullet_Start_m6BD187DD353835D248DA404B169DCE29CEB2B813,
	Bullet_Update_m5AA63D0B1F389C2CFEE77466E1C39ADC813B4DBC,
	Bullet_MoveTowardsEnemy_m314E141E2B73B185DFE36A68ACE6B523467E45E4,
	Bullet_OnTriggerEnter2D_mC3642C0546BCF5AFC8036C6B77D66560C04685E8,
	Bullet__ctor_m873C02F2114EA93A35E4392013AC831246756CBA,
	NULL,
	NULL,
	LaserShoot_Update_m0E1FEB6CD3EDB5879EDA2799F6A7378DAFD16AD3,
	LaserShoot_Draw2DRay_m4601948D0B966C169AD4925D6D68D257AD9753FE,
	LaserShoot_GetWaitingTime_mC6F58FFADCECF4628C7DB994BA8C8EDD6ED220C9,
	LaserShoot_SetWaitingTime_mE3431CA6BE152893760880A0556A124EE6B7CDBE,
	LaserShoot_CheckTargetVisible_m047378AC517F3B2781B7BEE12ED528301B09143C,
	LaserShoot_FindInArea_m7436C30E8B41D24F378E050BCE68C0FAB4D84AD1,
	LaserShoot_OnDrawGizmos_m0079DB1C68F35615DC7DE6B1C2344E08DB0C26C4,
	LaserShoot__ctor_m75F80B0B9962955972A16AB488BC53123AAD2A82,
	Shoot_Update_m8BAF074111542636D4F80A67B54B2AF3CFD14BFE,
	Shoot_GetWaitingTime_mDC4084B91ADB2911CAE4B2923F7E65A0124089EB,
	Shoot_SetWaitingTime_m43CDE754F6C6C5C48CAD566353822200341F65E8,
	Shoot_CheckTargetVisible_m2E8EFB91067667266086070DF809382B184D4438,
	Shoot_FindInArea_mF2865548A1865CB950ED8D91AED88BE86CE18B38,
	Shoot_OnDrawGizmos_m0C7D42FBC5144241E4E4383E6CBDF042F3FB104E,
	Shoot__ctor_m9CC0333342223937F83B34DB4109B3101CCB3AA9,
	MapShuffle__ctor_mC5DE537178AFFAD6C98DFDE0303CAEFAE2AB99A0,
	Key_Start_m4623E79D02D7B947893DC95D623501BA70CAEAE4,
	Key_CalculateFloorsWeights_m86607D9BA7634FD9C8FC8BB4615EC2A8E811D552,
	Key_GetRandomFloorIndex_m4A6B12AAB8AAA1CADEB0C23B27F63321AAB06767,
	Key_OnTriggerEnter2D_m75DD5CC1FA070E9F6602591A09F84EAEAB29B02D,
	Key__ctor_mA069FB77FE5BAEC833A31356AB0867315F988954,
	GameMaster_StartRun_m917A63CBE7FE01C1B4A5119D3F2CB690C4F3EF2E,
	GameMaster_Restart_mC98984119018C50CC697BF9A72164A0AFF29E2FE,
	GameMaster_GoToMenu_mF977278377E4B46D9AF65C7B059CE3DDCF480827,
	GameMaster__ctor_m129F9606E5D75C368420BE6B10EE572EE25E7780,
	Bomby_OnTriggerEnter2D_m536B3A91C82F378AE52DA03F18EC5F621290790F,
	Bomby__ctor_m1379BC4E1C157DB3BB5FF3FBD53869452EFDB93B,
	BombyCollector_OnTriggerEnter2D_mFD3DCCF7F141B7FFC5AB896C425ED80C9A5BCA4F,
	BombyCollector__ctor_m72CB35B822E79A8D5476CF5601DEB590DA292119,
	DoubleShot_Start_mFB5E1166873122BC225E0EADF20019BE352EB5BD,
	DoubleShot_OnTriggerEnter2D_m5559F17CC36F4EF8CD418BFB5B5E62831DAF7363,
	DoubleShot__ctor_mCD290AF55EF2110EE4B3121ACA31EE758DB1D8DC,
	HealthUp_OnTriggerEnter2D_m86EACED2C3332DAD38ED9568C04F95C8E4FEFA9F,
	HealthUp__ctor_mFB04723D1693953C7F06412875BB2AC600B0523A,
	TripleShot_Start_m241265F81A29A0C6F8F5168EABF523D9E0CDD75C,
	TripleShot_OnTriggerEnter2D_mFFAE6E3F2010F1A550729702DE5A92B5A65BDCDE,
	TripleShot__ctor_m9647E582D26A7217F8638CE49CFEB49172093D0E,
	Bot_Update_m149B128C75D5A0157556DD8FCE3460EC91A972BA,
	Bot_OnTriggerEnter2D_mE8DDF07E1C018777FA23297F550136BA2F48C9ED,
	Bot__ctor_m721C9F7B447E4CD482724E545EE797CD7762B2D8,
	HealthBar_SetMaxHealth_m83136EA47EA9BE342810EB1344D4E4E7E7459CFE,
	HealthBar_SetHealth_mC9D55DF6FDBB61FA5B5D05E3A3F8601F00045557,
	HealthBar__ctor_m6874A2796BC8D86E80B24E349500653ACFA80662,
	Player_GetDifficultyScale_mFD13FFF5BFC267C711C49E2911B007F3AFB5AAB4,
	Player_SetDifficultyScale_mFC60C2BBFAA5D5C51E3C272E7C8E746D07758585,
	Player__ctor_m0A83E0706592FC871B0CF188B37AFC6649F3D85D,
	PlayerHealth_Start_mB40232C538ABF2AAA435E94E0A697D0A0D25C6F5,
	PlayerHealth_Update_m5645A1624A67A8F9332B3DB67A4C2BAABF3DCD5A,
	PlayerHealth_TakeDamage_mB6C8B2F391600A980FC91EE61150A4C862E94071,
	PlayerHealth_EndDamageAnimation_m382586AE134BB51CD58634BCFA75D789E2824B68,
	PlayerHealth_PlayerDead_mB571483D37F07C7B6DE2BDDEBD4A2E29869BCBBE,
	PlayerHealth_Heal_mD3007EA4440F16CA78A148830089AF07CFEAE9F0,
	PlayerHealth__ctor_m6A07958FCBF285AA65AB66D48C3EB198068F37BE,
	ChromaticAberrationEffect_Start_m488FF2D4820FFD24303768A9B59EFD127853830A,
	ChromaticAberrationEffect_FixedUpdate_m29C2DED0C144B701F858C3A3E87CEC6E62EC6988,
	ChromaticAberrationEffect_SetLowHealthEffect_mA9ADB9F5D4F57E6772D34649D313BB007BFCA08C,
	ChromaticAberrationEffect__ctor_m708269EC7FB79186558A2550169280C7D87F6108,
	AbstractDungeonGenerator_generateDungeon_m33E47C0DE0A0BA31EDB5078796870F640AF42120,
	NULL,
	AbstractDungeonGenerator__ctor_mCE0757416D9F1F18B4D3CDC5A31FE679B1DA47C5,
	CorridorFirstDungeonGenerator_RunProceduralGeneration_m285E9BA24D4A8F739B38251ED8DA633EF627E393,
	CorridorFirstDungeonGenerator_CorridorFirstGeneration_mF9F424A46F8ECE9412C088821AC40CAB651BE04A,
	CorridorFirstDungeonGenerator_CreateCorridors_m0ADC4F2326FD51EF22D3545CAE388E859E6FDB5A,
	CorridorFirstDungeonGenerator_CreateRooms_m4F2085BB6B571B5BB0E3DC1AF39E072F0126175C,
	CorridorFirstDungeonGenerator_FindAllDeadEnds_m31C2BA90FC231A3B024A84DF538794E514098179,
	CorridorFirstDungeonGenerator_createRoomsAtDeadEnds_m3F9B390B8BA6C3E5AFF30C3451732E0E57905765,
	CorridorFirstDungeonGenerator_IncreaseCorridorBrush3by3_m8788F580761639954CB8866DAF182E9D32993250,
	CorridorFirstDungeonGenerator_IncreaseCorridorSizeByOne_mCB0F31851BF8309E3167978C0EA277F81F6635F0,
	CorridorFirstDungeonGenerator_GetDirection90From_mBD2A0195396DFF8A0EF7DED68FB50206DBA53E36,
	CorridorFirstDungeonGenerator__ctor_mA7C7FBED6DED660D4120FD06885049EF864AA5DC,
	U3CU3Ec__cctor_m109C4075252842EF4554EC34BEFAC0FAC31EBC47,
	U3CU3Ec__ctor_m95F885BD404F65400514146A50E67E0172F9F918,
	U3CU3Ec_U3CCreateRoomsU3Eb__9_0_mC68FA7163D8D0AD8D500E391E4177B22D24C0A6B,
	Graph__ctor_m0CC674B780084DEDA6A8B4AF1EC1A6D73AB67B3D,
	Graph_GetNeighbours4Directions_m1AB3E7336BBE2A73793738BD85702944BD76E980,
	Graph_GetNeighbours8Directions_m24D01B844493B8D9E50EC6770BCE12C4B81175E8,
	Graph_GetNeighbours_mDD56848A68B6C2EFA293352903DBA92CB7829184,
	Graph__cctor_mBF4BD31CB3515B47F131D17914A0E651EE25D17E,
	ItemPlacementHelper_CalculatePropsWeights_m22D70EEEAE512E6C311B41152B2A99354A37EE55,
	ItemPlacementHelper_GetRandomPropIndex_m9BAFE4558B07436584EF573F0E092D3AFFD43B8D,
	ItemPlacementHelper_CalculatePerksWeights_m74006E959AD9F19929A5314C1CC1178AA698267D,
	ItemPlacementHelper_GetRandomPerkIndex_mF38AE9355B216D395EB998793C215545D8FC11C5,
	ItemPlacementHelper_ItemPlacementHelperMethod_mDF744180E56252B8828628F12903D7314AA06269,
	ItemPlacementHelper_PlaceItems_m7D0E0134F29B21FE238861723FB69409613979AC,
	ItemPlacementHelper_PlaceEnemies_mF22397FC02447E3FF35F8C1A4A7561CDD17617C2,
	ItemPlacementHelper_PlaceItemsBossRoom_mEBCC494932C7375F4F20994A5D8ECBC9B589CE88,
	ItemPlacementHelper_PlaceItemsTreasureRoom_m1A1284AA343BA48D004B2849A5B6D18C45243909,
	ItemPlacementHelper_GetItemPlacementPosition_m4FD2B2DDFB24EBA36A346F5AF83A41F5EC01DD9E,
	ItemPlacementHelper_PlaceBigItem_m53C12D3F1675E2605AE8642251881E8C383A6B78,
	ItemPlacementHelper__ctor_mB1053EAD2C21A54B13FDAF9CE54F4085108D2825,
	ProceduralGenerationAlgorithms_SimpleRandomWalk_mA86E251C63A809EB0C0D9F8817B3FBA3FD37D8CA,
	ProceduralGenerationAlgorithms_RandomWalkCorridor_mDD5EE35A69092651CDEF64D301AD0B16A89FE83F,
	ProceduralGenerationAlgorithms_BinarySpacePartitioning_mB5110F742C19CD05DA8DA39AFC5A78AC115DA041,
	ProceduralGenerationAlgorithms_SplitVertically_m057DA90B738EF47E423792F4BDE48A72EC6E1D14,
	ProceduralGenerationAlgorithms_SplitHorizontally_m7647279A23D579E24ACFAE9553B1DE797633C5DA,
	Direction2D_GetRandomCardinalDirection_m901DCB552E09E731ECC3D725219855F2A5F643C3,
	Direction2D__cctor_mFA24083F7C1303673AB4E278A748C49FD09B6291,
	Prop__ctor_m1A400EF58C3126BE12D9F4EC6677CE98B3A7B68C,
	PropObject__ctor_m59E31F445F024D72E44520B0261767BD49CDFE96,
	Perk__ctor_mC2121DCCC26538A8E812913855FD3EDC60574599,
	RoomFirstDungeonGenerator_RunProceduralGeneration_m8A744CC1E15A8F350C06FA127FC2F8AC41BFDD0A,
	RoomFirstDungeonGenerator_Start_mA5F92B643D924EF3F2957F8062023E68C233FC69,
	RoomFirstDungeonGenerator_ClearAll_m26E5CB4F2D95A3FB31511556FF2CAB3015FA6E70,
	RoomFirstDungeonGenerator_CreateRooms_mB5ED554CF6ECE2817143FA596F1D353217089347,
	RoomFirstDungeonGenerator_CreateSimpleRooms_m8FDFB429853180AD2086F8D1A5B1FD20653DDCF6,
	RoomFirstDungeonGenerator_ConnectRooms_m25C56F22BDB247E12BA46E54B786BF3D1F63FBE4,
	RoomFirstDungeonGenerator_FindClosestPointTo_m7C3284DB259D6791026BDBA2DC34E40432E69637,
	RoomFirstDungeonGenerator_CreateCorridor_mF7D2EF8E14C44FB52AC3BDB6E7A561DE3CA23659,
	RoomFirstDungeonGenerator_SpawnPlayer_m8B51B268B805745B862DA85CBE3BB6A487180484,
	RoomFirstDungeonGenerator_IncreaseCorridorSize_m4D3C35252832C4A6C5BBB009025A129E6625C987,
	RoomFirstDungeonGenerator__ctor_m4D3C06DD867BFE1C1ED46C2F72DA91D9A4BEAA1A,
	SimpleRandomWalkMapGenerator_RunProceduralGeneration_m0647C1D0F5F981CA414170B99B00B425E2759C74,
	SimpleRandomWalkMapGenerator_RunRandomWalk_m53DFBA6F75960C52C21FB38CC9CA8188CFB22AE8,
	SimpleRandomWalkMapGenerator__ctor_m9A17F3808C2E6040C37AFFBB169C310AC8F3518C,
	Floor__ctor_mC6DD1BA9604B35FFF0346F67AA3D390F4C2439BF,
	TilemapVisualizer_Awake_m41E05327E14B2C073ECBE54207A3C1B0682DB762,
	TilemapVisualizer_GetRandomFloorIndex_mCD55AE14DC05DFC62D814F2823F391105E52F257,
	TilemapVisualizer_CalculateFloorWeights_m5E58C6DEE2FB5C012DC3749BA8B64860951CB535,
	TilemapVisualizer_PaintFloorTiles_mA7DFAC26723F24E9AF3C2C8A908A3358DF8DDBDD,
	TilemapVisualizer_PaintTiles_mAEA1043A45B5803065E0A1E6372C591ED0493481,
	TilemapVisualizer_PaintSingleTile_m02572F581FF4DB5D993C60CBE47266A4E5A0EB36,
	TilemapVisualizer_PaintSingleBasicWall_mCD34D4B47C1840E29E6D81EAEFEBE32954571107,
	TilemapVisualizer_PaintSingleCornerWall_m1914D8596949EC2AA559379217616BEA4848286D,
	TilemapVisualizer_Clear_mA94CE31652588043DADF56CC4739F3BB21968A40,
	TilemapVisualizer__ctor_mFFEC8C1476753A323D801584347257CB00740278,
	WallGenerator_CreateWalls_mE5288EEA53D08F56C2A6BC7AC1CC39F6A4D01C83,
	WallGenerator_CreateBasicWalls_m972B9E06431C61DF8308524C7DFE501CA96B6F33,
	WallGenerator_CreateCornerWalls_mEE4872F704D73B0D0B86541C2358C7BF97486D0A,
	WallGenerator_FindWallsInDirections_m42CD3AD9D4DE37EA80270F18A72172FB0D9E2E6C,
	WallTypesHelper__cctor_m9E1D60C507AD9C523D9BFD6E3CC0ADA93724AACE,
	Chest_Start_m13FCC9702F8A7CC4FEA2847E6E3CAB7A635B1DCF,
	Chest_TakeDamage_m189E71FBA172DF441FA974FEA2D9895AC30D81E9,
	Chest__ctor_m2D31AFE0D73A6DF61B1F7EAAB809D3E1F6FF579A,
	Coin_Start_mA073768C06B75276A262C088D118DEEE9F40991E,
	Coin_Update_mD81A56D3C82C2FF349BB57ED6C8B95699F2E5908,
	Coin_OnTriggerEnter2D_m3983AB890902F168831629F16D99A3C0A6160448,
	Coin__ctor_mFEAAC42C1DAABB5CCCF4B39FCDBC5D0B0B8A183D,
	CoinSystem_Start_m1E1E87A571602BF33CB8D7950B333E785779186D,
	CoinSystem__ctor_m91E7A20F6037BCC110349B4621A04F70C91DD24D,
	Magnet_OnTriggerEnter2D_mA988BB37218E5DC4FA79815D1EE748EC366FA749,
	Magnet__ctor_m3B1CFB312BF10846408B5F0255B5E76C747B1812,
	LightningBoltScript_GetPerpendicularVector_m6E14CE8B174161B94706CE092FF3BE4049C53983,
	LightningBoltScript_GenerateLightningBolt_m6F45F784BE0610F9E9B519C5E290D3515DE0BA49,
	LightningBoltScript_RandomVector_m8D5CB399D3CE0038571D41DE824CE673E766B4FA,
	LightningBoltScript_SelectOffsetFromAnimationMode_m98F0C804C05EFB6BC3283AC50D654D5A672B6199,
	LightningBoltScript_UpdateLineRenderer_mC709724D71E0E95242108C4B9D6F7DA6BAB71F65,
	LightningBoltScript_Start_m980C378E5754957B1F4DE221515969B6A328240E,
	LightningBoltScript_Update_m0BE2BF4FB4CD027E8C78FE58991FFF103836AF31,
	LightningBoltScript_Trigger_mA1EDB81D4D9001D2B19FFBACB0A36BAFEE96C284,
	LightningBoltScript_UpdateFromMaterialChange_m977EBCF7F3B1B47A777F34B0305C7FC8A04E1EF5,
	LightningBoltScript__ctor_m6B404ACF4FA73DFC000CE86270A4409AA1BBDD42,
};
static const int32_t s_InvokerIndices[280] = 
{
	5882,
	2149,
	6011,
	9275,
	9275,
	6011,
	4802,
	6011,
	5794,
	5882,
	6011,
	5882,
	6011,
	6011,
	4802,
	4802,
	4737,
	4737,
	6011,
	6011,
	5938,
	5938,
	5999,
	5938,
	4880,
	5938,
	4880,
	5855,
	4802,
	5794,
	4737,
	5794,
	4737,
	6011,
	4828,
	4828,
	1060,
	6011,
	2186,
	4828,
	4500,
	6011,
	5938,
	4880,
	6011,
	4828,
	4828,
	1060,
	6011,
	6011,
	6011,
	4828,
	4828,
	6011,
	5938,
	4880,
	4802,
	6011,
	4828,
	4828,
	1060,
	6011,
	6011,
	6011,
	6011,
	2812,
	6011,
	6011,
	6011,
	6011,
	6011,
	6011,
	5794,
	4737,
	5882,
	4828,
	6011,
	6011,
	5882,
	5882,
	6011,
	6011,
	5794,
	6011,
	5882,
	6011,
	6011,
	4828,
	4802,
	6011,
	4802,
	6011,
	5794,
	5882,
	6011,
	5882,
	5794,
	4737,
	5882,
	4828,
	6011,
	6011,
	5882,
	5882,
	6011,
	6011,
	5794,
	6011,
	5882,
	6011,
	6011,
	4828,
	4802,
	6011,
	4802,
	6011,
	5794,
	5882,
	6011,
	5882,
	4828,
	6011,
	6011,
	6011,
	4828,
	4828,
	6011,
	0,
	0,
	6011,
	2838,
	5938,
	4880,
	5794,
	6011,
	6011,
	6011,
	6011,
	5938,
	4880,
	5794,
	6011,
	6011,
	6011,
	6011,
	6011,
	4828,
	4061,
	4828,
	6011,
	6011,
	6011,
	6011,
	6011,
	4828,
	6011,
	4828,
	6011,
	6011,
	4828,
	6011,
	4828,
	6011,
	6011,
	4828,
	6011,
	6011,
	4828,
	6011,
	4802,
	4802,
	6011,
	5938,
	4880,
	6011,
	6011,
	6011,
	4802,
	6011,
	6011,
	4802,
	6011,
	6011,
	6011,
	4737,
	6011,
	6011,
	0,
	6011,
	6011,
	6011,
	2149,
	4313,
	4313,
	2762,
	4313,
	4313,
	4503,
	6011,
	9275,
	6011,
	3803,
	4828,
	4327,
	4327,
	2161,
	9275,
	4828,
	4061,
	4828,
	4061,
	96,
	1382,
	1390,
	1471,
	1471,
	539,
	1084,
	6011,
	8252,
	8252,
	7515,
	7831,
	7831,
	9271,
	9275,
	6011,
	6011,
	6011,
	6011,
	6011,
	6011,
	6011,
	4313,
	4313,
	2210,
	2162,
	2210,
	4313,
	6011,
	6011,
	856,
	6011,
	6011,
	6011,
	5855,
	6011,
	4828,
	1448,
	1454,
	2839,
	2839,
	6011,
	6011,
	8548,
	7864,
	7864,
	8227,
	9275,
	6011,
	4802,
	6011,
	6011,
	6011,
	4828,
	6011,
	6011,
	6011,
	4828,
	6011,
	2239,
	534,
	906,
	6011,
	6011,
	6011,
	6011,
	6011,
	6011,
	6011,
};
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	280,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
