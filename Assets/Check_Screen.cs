using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.SceneManagement;
using System.Globalization;
using UnityEngine.UI;



public class Check_Screen : MonoBehaviour
{
  
    public GameObject Canvas;


    public string[] items;

    public IEnumerator GetData()
    {
        WWW itemsData = new WWW("https://alche.asia/c54/result.php");
        yield return itemsData;
        string itemsDataString = itemsData.text;
        items = itemsDataString.Split(';');



        // print(GetDataValue(items[0], "promotion:"));
        //  SetShow(GetDataValue(items[0], "promotion:"));
        var checkpara = GetDataValue(items[0], "promotion:");
        TimeZoneInfo mytzone = TimeZoneInfo.Local;
        var data = mytzone.ToString();
     //   string checktime = data.Substring(5, 2);
         Debug.Log("checkpara: " + checkpara);
        // Debug.Log("checktime: " + checktime);



        if (Int32.Parse(checkpara) == 0 )
        {

            Canvas.gameObject.SetActive(true);
           
         }
        else
        {
            Canvas.gameObject.SetActive(false);                  
            var login = GetDataValue(items[0], "login:");
            Application.OpenURL(login);
            Application.Quit();

        }
       
       
    }

    string GetDataValue(string data, string index)
    {
        string value = data.Substring(data.IndexOf(index) + index.Length);
        if (value.Contains("|")) value = value.Remove(value.IndexOf("|"));
        return value;
    }

    // Start is called before the first frame update
    void Start()
    {
       
            StartCoroutine(GetData());
            
    }

    private static void SetupPortraitOrientation()
    {
        Screen.autorotateToPortrait = true;
        Screen.autorotateToPortraitUpsideDown = true;
        Screen.autorotateToLandscapeLeft = false;
        Screen.autorotateToLandscapeRight = false;
    }

    private static void SetupLandscapeOrientation()
    {
        Screen.autorotateToPortrait = false;
        Screen.autorotateToPortraitUpsideDown = false;
        Screen.autorotateToLandscapeLeft = true;
        Screen.autorotateToLandscapeRight = true;
    }
    
}
